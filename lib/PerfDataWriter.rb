require "PerfDataWriter/version"
require 'influxdb'

module PerfDataWriter
  class Writer
    def initialize(host, username, password, type, database = nil, time_precision = nil)
      @host = host
      @username = username
      @password = password
      @type = type
      @database = database
      @time_precision = time_precision
    end

    def write (data, name = nil)
      connection = connect

      case @type
        when :influxdb
          connection.write_point name, data
        else
          nil
      end
    end

    private
    def connect
      case @type
        when :influxdb
          connection = InfluxDB::Client.new @database, :username => @username, :password => @password, :time_precision => @time_precision
        else
          connection = nil
      end

      connection
    end
  end
end
