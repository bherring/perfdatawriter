# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'PerfDataWriter/version'

Gem::Specification.new do |spec|
  spec.name          = "PerfDataWriter"
  spec.version       = PerfDataWriter::VERSION
  spec.authors       = ["Brad Herring"]
  spec.email         = ["bradley.herring@cgifederal.com"]
  spec.summary       = "Classes to write Nagios performance data to time databases"
  spec.description   = "Classes to write Nagios performance data to time databases"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "influxdb"

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
end
